FROM ruby:2.7.3

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq && apt-get install -y build-essential yarn libpq-dev libpq5

RUN cd /usr/local && curl -o - https://nodejs.org/dist/v14.17.3/node-v14.17.3-linux-x64.tar.xz | tar -Jxvf - 
RUN echo 'export PATH=/usr/local/node-v14.17.3-linux-x64/bin:$PATH' >> /etc/profile

ENV APP_HOME /app
ENV RAILS_ENV production

RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD . $APP_HOME
RUN gem install bundler
RUN bundle install

RUN rails webpacker:install
RUN rails assets:precompile
#RUN yarn install --check-files

VOLUME /app/storage

EXPOSE 3000

CMD ["rails","server","-b","0.0.0.0"]
