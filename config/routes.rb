Rails.application.routes.draw do

  root to: "articles#index"    
  
  post '/sessions', to: 'sessions#create'
  put '/sessions', to: 'sessions#create'
  patch '/sessions', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'
  get 'sessions/new', to: 'sessions#new'

  resources :articles do
      resources :comments
  end
    
  resources :users

end
