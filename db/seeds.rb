# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


user1 = User.create(name: "Coder 1", username: "coder1", password: "123456")

user1.articles.create(title: "Why use ruby",
                      status: :public,
                      body: %/Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis tristique dolor. Integer tristique ipsum pharetra odio facilisis, imperdiet vehicula ante congue. Aliquam pulvinar finibus facilisis. Duis feugiat suscipit eros. Nulla quis eros ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam sollicitudin euismod aliquet. Morbi quis odio non ipsum ultrices facilisis nec quis augue. Quisque congue, magna sed finibus volutpat, arcu velit tempor velit, sed dictum quam augue ut magna. Vivamus tempus, orci id condimentum accumsan, tortor dolor finibus ipsum, sed consequat justo sem eget lacus. Vestibulum iaculis nisi nec augue fermentum, a pretium ante euismod. Duis interdum eleifend varius.

Phasellus et turpis eu felis congue malesuada vitae nec nulla. Sed faucibus id orci sit amet porta. Aenean justo metus, venenatis sed urna vitae, vestibulum finibus leo. Phasellus et dolor id elit pulvinar vulputate ut at lectus. Nullam eget justo vel odio congue pretium at nec lacus. Pellentesque nec metus non mauris rhoncus euismod. Praesent suscipit interdum nisl non efficitur. Suspendisse consequat orci eget ipsum faucibus, ut iaculis arcu vehicula.

Aenean dapibus a ipsum vel ultricies. Vivamus sagittis nibh sit amet elementum pharetra. Phasellus a turpis vulputate nunc lacinia pharetra quis vitae odio. Sed commodo dignissim commodo. Ut lobortis quam consectetur, condimentum magna sit amet, porttitor elit. Nulla facilisi. Aenean vel bibendum nulla, vitae sodales mauris. Nunc et ullamcorper dolor, vitae blandit lacus. Donec egestas, mi eu cursus commodo, sem justo mollis enim, ac vehicula lectus nulla at neque. Cras turpis ex, egestas id imperdiet ullamcorper, ultricies ac augue. Vivamus finibus justo sit amet magna dignissim scelerisque sed nec augue. Nunc efficitur felis non magna feugiat laoreet. Praesent fringilla eget magna ut ullamcorper./)

user2 = User.create(name: "Coder 2", username: "coder2", password: "123456")


user2.articles.create(title: "How to deploy rails app with Capistrano",
                      status: :public,
                      body: %/Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis tristique dolor. Integer tristique ipsum pharetra odio facilisis, imperdiet vehicula ante congue. Aliquam pulvinar finibus facilisis. Duis feugiat suscipit eros. Nulla quis eros ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam sollicitudin euismod aliquet. Morbi quis odio non ipsum ultrices facilisis nec quis augue. Quisque congue, magna sed finibus volutpat, arcu velit tempor velit, sed dictum quam augue ut magna. Vivamus tempus, orci id condimentum accumsan, tortor dolor finibus ipsum, sed consequat justo sem eget lacus. Vestibulum iaculis nisi nec augue fermentum, a pretium ante euismod. Duis interdum eleifend varius.

Phasellus et turpis eu felis congue malesuada vitae nec nulla. Sed faucibus id orci sit amet porta. Aenean justo metus, venenatis sed urna vitae, vestibulum finibus leo. Phasellus et dolor id elit pulvinar vulputate ut at lectus. Nullam eget justo vel odio congue pretium at nec lacus. Pellentesque nec metus non mauris rhoncus euismod. Praesent suscipit interdum nisl non efficitur. Suspendisse consequat orci eget ipsum faucibus, ut iaculis arcu vehicula.

Aenean dapibus a ipsum vel ultricies. Vivamus sagittis nibh sit amet elementum pharetra. Phasellus a turpis vulputate nunc lacinia pharetra quis vitae odio. Sed commodo dignissim commodo. Ut lobortis quam consectetur, condimentum magna sit amet, porttitor elit. Nulla facilisi. Aenean vel bibendum nulla, vitae sodales mauris. Nunc et ullamcorper dolor, vitae blandit lacus. Donec egestas, mi eu cursus commodo, sem justo mollis enim, ac vehicula lectus nulla at neque. Cras turpis ex, egestas id imperdiet ullamcorper, ultricies ac augue. Vivamus finibus justo sit amet magna dignissim scelerisque sed nec augue. Nunc efficitur felis non magna feugiat laoreet. Praesent fringilla eget magna ut ullamcorper./)

user3 = User.create(name: "Coder 3", username: "coder3", password: "123456")


user3.articles.create(title: "Rails 6 with Docker",
                      status: :public,
                      body: %/Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis tristique dolor. Integer tristique ipsum pharetra odio facilisis, imperdiet vehicula ante congue. Aliquam pulvinar finibus facilisis. Duis feugiat suscipit eros. Nulla quis eros ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam sollicitudin euismod aliquet. Morbi quis odio non ipsum ultrices facilisis nec quis augue. Quisque congue, magna sed finibus volutpat, arcu velit tempor velit, sed dictum quam augue ut magna. Vivamus tempus, orci id condimentum accumsan, tortor dolor finibus ipsum, sed consequat justo sem eget lacus. Vestibulum iaculis nisi nec augue fermentum, a pretium ante euismod. Duis interdum eleifend varius.

Phasellus et turpis eu felis congue malesuada vitae nec nulla. Sed faucibus id orci sit amet porta. Aenean justo metus, venenatis sed urna vitae, vestibulum finibus leo. Phasellus et dolor id elit pulvinar vulputate ut at lectus. Nullam eget justo vel odio congue pretium at nec lacus. Pellentesque nec metus non mauris rhoncus euismod. Praesent suscipit interdum nisl non efficitur. Suspendisse consequat orci eget ipsum faucibus, ut iaculis arcu vehicula.

Aenean dapibus a ipsum vel ultricies. Vivamus sagittis nibh sit amet elementum pharetra. Phasellus a turpis vulputate nunc lacinia pharetra quis vitae odio. Sed commodo dignissim commodo. Ut lobortis quam consectetur, condimentum magna sit amet, porttitor elit. Nulla facilisi. Aenean vel bibendum nulla, vitae sodales mauris. Nunc et ullamcorper dolor, vitae blandit lacus. Donec egestas, mi eu cursus commodo, sem justo mollis enim, ac vehicula lectus nulla at neque. Cras turpis ex, egestas id imperdiet ullamcorper, ultricies ac augue. Vivamus finibus justo sit amet magna dignissim scelerisque sed nec augue. Nunc efficitur felis non magna feugiat laoreet. Praesent fringilla eget magna ut ullamcorper./)
