class Article < ApplicationRecord
   
   include Visible
   
   belongs_to :user
   alias_attribute :author, :user
   has_many :comments, dependent: :destroy
    
   validates :title, presence: true
   validates :body, presence: true, length: { minimum: 10 }
    
   VALID_STATUSES = ['public', 'private', 'archived']

   validates :status, inclusion: { in: VALID_STATUSES }


end
