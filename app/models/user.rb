class User < ApplicationRecord

    has_many :articles

    validates :name, presence: true
    validates :password, presence: true
    validates :username, uniqueness: true
end
