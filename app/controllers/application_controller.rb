class ApplicationController < ActionController::Base
    
    before_action :set_user
    
    private
    
    def set_user
        if session.include?(:uid) != nil
            @current_user = User.find_by(id: session[:uid])
        end
    end
    
end
