class SessionsController < ApplicationController
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.find_by(username: user_params[:username])
    
    if @user and @user.password == user_params[:password]
      session[:uid] = @user.id
      redirect_to articles_path
    else
      @user = User.new
      @error = "Usuário ou senha inválidos"
      render 'new'
    end
  end
  
  def destroy
    session[:uid] = nil
    @user = nil
    redirect_to articles_path
  end
  
  private
  
  def user_params
    params.require(:user).permit(:username, :password)
  end
end
